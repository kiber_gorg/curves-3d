import geometry.Curve;
import geometry.implementation.Circle;

import java.util.function.Predicate;

public class CircleFilter implements Predicate<Curve> {

    @Override
    public boolean test(Curve curve) {
        return curve instanceof Circle;
    }
}
