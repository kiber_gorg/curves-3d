import geometry.*;
import geometry.implementation.Circle;
import geometry.implementation.Ellipse;
import geometry.implementation.Helix;

import java.util.Random;

public class CurveGenerator {
    private final Random rnd = new Random();
    public Curve generateCurve() {
        /*Map<Curves, Supplier<Curve>> map = Map.of(
                Curves.CIRCLE, this::generateCircle,
                Curves.ELLIPSE, this::generateEllipse,
                Curves.HELIX, this::generateHelix);
        return map.get(Curves.random()).get();*/
        return switch (Curves.random()) {
            case Curves.CIRCLE -> generateCircle();
            case Curves.ELLIPSE -> generateEllipse();
            case Curves.HELIX -> generateHelix();
        };
    }

    private Curve generateCircle() {
        return new Circle(
                rnd.nextDouble(0.5, 3));
    }
    private Curve generateEllipse() {
        return new Ellipse(
                rnd.nextDouble(0.5,3),
                rnd.nextDouble(0.5, 3));
    }
    private Curve generateHelix() {
        return new Helix(
                rnd.nextDouble(0.5, 3),
                rnd.nextDouble(0.5, 3));
    }
}
