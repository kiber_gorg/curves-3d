package geometry;

public interface Curve {
    Vector getDerivative(double param);
    Vector getPoint(double param);
}
