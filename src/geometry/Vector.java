package geometry;

import java.util.Arrays;

public class Vector {
    private final double[] e = new double[3];
    public Vector(double x, double y, double z) {
        e[0] = x;
        e[1] = y;
        e[2] = z;
    }

    @Override
    public String toString() {
        return Arrays.toString(e);
    }
}
