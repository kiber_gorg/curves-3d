package geometry;

import java.util.Random;

public enum Curves {
    CIRCLE,
    ELLIPSE,
    HELIX;

    public static Curves random() {
        return switch (new Random().nextInt(3)) {
            case 1 -> ELLIPSE;
            case 2 -> HELIX;
            default -> CIRCLE;
        };
    }
}
