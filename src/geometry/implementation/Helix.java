package geometry.implementation;

import geometry.Curve;
import geometry.Vector;

public class Helix implements Curve {
    double radius;
    double step;

    public Helix(double radius, double step) {
        this.radius = radius;
        this.step = step;
    }

    @Override
    public Vector getDerivative(double param) {
        return new Vector(
                radius*Math.cos(param),
                radius*Math.sin(param),
                step/(2*Math.PI));
    }

    @Override
    public Vector getPoint(double param) {
        return new Vector(
                radius*Math.cos(param),
                radius*Math.sin(param),
                step*param/(2*Math.PI));
    }
}
