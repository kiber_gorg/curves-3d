package geometry.implementation;

import geometry.Curve;
import geometry.Vector;

public class Circle implements Curve {
    double radius;

    public Circle(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    @Override
    public Vector getDerivative(double param) {
        return new Vector(
                radius*Math.sin(param)*(-1),
                radius*Math.cos(param),
                0);
    }

    @Override
    public Vector getPoint(double param) {
        return new Vector(
                radius*Math.cos(param),
                radius*Math.sin(param),
                0);
    }
}
