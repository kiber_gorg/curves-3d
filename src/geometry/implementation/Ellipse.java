package geometry.implementation;

import geometry.Curve;
import geometry.Vector;

public class Ellipse implements Curve {
    double radiusX;
    double radiusY;

    public Ellipse(double radiusX, double radiusY) {
        this.radiusX = radiusX;
        this.radiusY = radiusY;
    }

    @Override
    public Vector getDerivative(double param) {
        return new Vector(
                radiusX*Math.sin(param)*(-1),
                radiusY*Math.cos(param),
                0);
    }

    @Override
    public Vector getPoint(double param) {
        return new Vector(
                radiusX*Math.cos(param),
                radiusY*Math.sin(param),
                0);
    }
}
