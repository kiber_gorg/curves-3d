import geometry.Curve;
import geometry.implementation.Circle;

import java.util.ArrayList;
import java.util.List;

public class Utils {
    public static List<Curve> generateContainer(int size) {
        CurveGenerator cg = new CurveGenerator();
        List<Curve> curves = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            curves.add(cg.generateCurve());
        }
        return curves;
    }

    public static <T extends Curve> void printCoordinates(List<T> curves, double param) {
        for (T curve : curves) {
            System.out.println(
                "point: "
                + curve.getPoint(param)
                + ", derivative: "
                + curve.getDerivative(param)
            );
        }
    }

    public static List<Circle> filterCircles(List<Curve> curves) {
        CircleFilter circleFilter = new CircleFilter();
        return curves.stream()
                .filter(circleFilter)
                .map(Circle.class::cast)
                .sorted(Utils::compareCircles)
                .toList();
    }
    private static int compareCircles(Circle lhv, Circle rhv) {
        return Double.compare(lhv.getRadius(), rhv.getRadius());
    }
}
