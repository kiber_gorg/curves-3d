import geometry.Curve;
import geometry.implementation.Circle;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        int size = 10;

        List<Curve> curves = Utils.generateContainer(size);
        Utils.printCoordinates(curves, Math.PI/4);

        System.out.println();

        List<Circle> circles = Utils.filterCircles(curves);
        Utils.printCoordinates(circles, Math.PI/4);
        double sum = circles.stream().mapToDouble(Circle::getRadius).sum();
        System.out.println(sum);
    }
}
